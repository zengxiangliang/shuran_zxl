$(function() {
	//变量区
	var time = null;
	var position = 0;
	var _index1 = 0;
	/**
	 * 轮播函数
	 */
	var swich_banner = function() {
		$(".big_banner img").hide();
		$(".big_banner img").eq(position).show();
		$(".small_icon .icon").css("background-position", " -45px -330px");
		$(".small_icon .icon").eq(position).css("background-position", "-0px -330px");
		position = (position + 1) % $(".big_banner img").size();
	};
	//通过setInterval实现轮播
	time = setInterval(swich_banner, 3000);
	/**
	 * 移入大图区域停止轮播
	 */
	$(".big_banner img,.direction").hover(
		function() {
			//停止轮播
			clearInterval(time);
		},
		function() {
			//移出继续轮播
			time = setInterval(swich_banner, 3000);
		}
	);
	/**
	 * 移入#banner时出现箭头，移出消失。
	 */
	$("#banner").hover(
		function() {
		//时间实现渐入效果
			$(this).find(".direction").show(100);
		},
		function() {
			$(this).find(".direction").hide(100);
		}
	);
	/**
	 * 移入小圆点到指定位置，并且改变颜色
	 */
	$(".small_icon .icon").hover(
		function() {
			clearInterval(time); //先暂停，再找得到相应的位置，
			var pos = $(this).index($(".small_icon .icon"));
			position = pos;
			swich_banner();
		},
		function() {
			//移出继续轮播
			time = setInterval(swich_banner, 3000);
		});
	/**
	 * 左右点击效果
	 */
	$(".right-direction").click(swich_banner);
	$(".left-direction").click(function() {
		clearInterval(time);
		position = (position - 2) % $(".big_banner img").size();
		swich_banner();
	});

	//content部分
	$(".serviceBlocks .publicity").hover(
		function() {
			$(this).addClass("publicityhover");
		},
		function() {
			$(this).removeClass("publicityhover");
		}
	);
	$(".serviceBlocks .ad").hover(
		function() {
			$(this).addClass("adhover");
		},
		function() {
			$(this).removeClass("adhover");
		}
	);
	$(".serviceBlocks .movies").hover(
		function() {
			$(this).addClass("movieshover");
		},
		function() {
			$(this).removeClass("movieshover");
		}
	);
	$(".serviceBlocks .others").hover(
		function() {
			$(this).addClass("othershover");
		},
		function() {
			$(this).removeClass("othershover");
		}
	);
	//下面是caseWrap
	/**
	 * 鼠标移入时箭头变背景
	 */
	$(".icon_left").hover(
		function() {
			//改变background-position即可
			$(this).css("background-position", "-90px -330px");
		},
		function() {
			$(this).css("background-position", "-90px -360px");
		}
	);
	$(".icon_right").hover(
		function() {
			$(this).css("background-position", "-135px -330px");
		},
		function() {
			$(this).css("background-position", "-135px -360px");
		}
	);
	//下面是小图点击变移动三格。
	// var lenght = 150;
	//var pos = 0;
	// $(".icon_left").click(function(){
	// 	var pos = (pos-3)%$("#caseWrap .case_list li").lenght;
	// 	$(this).parent().find("li").animate({left:"lenght*pos+'px'"},"slow");
	// });
	// $(".icon_right").click(function(){
	// 	var pos = (pos+3)%$("#caseWrap .case_list li").length;
	// 	$(this).parent().find("li").animate({left:"'-'+lenght*pos+'px'"},"slow");
	// });
	// 
	// 下面是show方法
	// 		first=0;
	// 		last=4;
	// 		touch=4;
	// var move = function(){
	// 	$("#caseWrap .case_list li").hide();
	// 	$("#caseWrap .case_list li").slice(first,last).show(1000);
	// }
	// $(".icon_right").click(
	// 	function(){
	// 	first+=touch;
	// 	last+=touch;
	// 	move();
	// 	}
	// );
	// $(".icon_left").click(
	// 	function(){
	// 		first-=touch;
	// 		last-=touch;
	// 		move();
	// 	}
	// );
	//  var n = 1;
	// function move(step){//step控制左移还是右移
	// 		var left = step*n*600;
	// 		var onleft = left+'px';
	// 	$("#caseWrap .case_list ul").animate({left:onleft},1000);
	// }
	// //点击右边按钮实现轮播
	// $(".icon_right").click(
	// 	function(){
	// 		 var n = 1;
	// 		n++;
	// 		move(-1);
	// });
	// //点击左边按钮实现轮播
	// $(".icon_left").click(
	// 	function(){
	// 		 var n = 1;
	// 		n++;
	// 		move(1);
	// });

	/**
	 * 下面是点击小箭头向左、向右滑动四格的方法。
	 */
	//右边按钮
	$('.icon_right').click(function() {
		_index1++;
		if (_index1 > 3) { //当位置大于3时归零。
			_index1 = 0;
			$('#caseWrap .case_list ul').animate({
				left: -_index1 * 600
			}, 500);
		} else {
			$('#caseWrap .case_list ul').animate({
				left: -_index1 * 600
			}, 500);
		}
	});

	//左边按钮
	$('.icon_left').click(function() {
		_index1--;
		if (_index1 < 0) {
			_index1 = 3; //当向前点时，位置调到最后一个。
			$('#caseWrap .case_list ul').animate({
				left: -_index1 * 600
			}, 500);
		} else {
			$('#caseWrap .case_list ul').animate({
				left: -_index1 * 600
			}, 500);
		}
	});
	/**
	 * 鼠标移入时颜色变红
	 */
	$(".contact li a").hover(
		function() {
			$(this).css("color", "#f00");
		},
		function() {
			$(this).css("color", "#000");
		}
	);
	/**
	 * 固定联系方式处动画；
	 * 根据点击left_fixed来通过animate控制
	 */
	$(".left_fixed").toggle(
		function() {
			$(this).parent().animate({
				right: "0px"
			}, "slow");
		},
		function() {
			$(this).parent().animate({
				right: "-129px"
			}, "slow");
		}
	);
});