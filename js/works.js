$(function() {
	/**
	 * 移入图片区域改变边框颜色和下方字体颜色
	 */
	$(".content_brief td a").hover(
		function() {
			$(this).css("color", "#f7141a");
			$(this).find("img").css("border-color", "#f7141a");
		},
		function() {
			$(this).css("color", "#666");
			$(this).find("img").css("border-color", "#eee");
		});
});