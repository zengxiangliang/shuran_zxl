$(function(){
	/**
	 *这是上面的hover
	 */
	// var timein,timeout;
	$(".nav li").hover(function(){
		$(this).children("a").addClass("hover");
		$(this).children("dl").css("display","block")
	},
	function(){
		$(this).children("a").removeClass("hover");
		$(this).children("dl").css("display","none");

	});
	// 
	// 通过控制hover事件的时间，有点问题，当鼠标移入里面的时候会出现闪动。
	// 
	// $(".nav li").each(function(){
	// 	var touch=$(this);
	// 	$(this).mouseenter(function(){
	// 		clearTimeout(timein);
	// 			var timein = setTimeout(
	// 			function(){
	// 			touch.children("a").addClass("hover");
	// 			$(".nav li dl").hide();
	// 			touch.children("dl").show();
	// 		},100);
	// 		}).mouseleave(function(){
	// 			clearTimeout(timeout);
	// 		    var timeout = setTimeout(
	// 		 	function(){
	// 		 	touch.children("a").removeClass("hover");
	// 		 	touch.children("dl").hide();
	// 		 	},200);
	// 		});
	// });
	/**
	 *这是移入改变背景颜色和文字颜色
	 */
	$(".nav dd,li.sidebar_list").hover(function(){
		$(this).css("background-color","#f6f6f6").find("a").addClass("hover");
	},
	function(){
		$(this).css("background-color","#fff").find("a").removeClass("hover");
	});
	/**
	 * 通过筛选是否是最后一个,给最后一个添加颜色。
	 */
	$(".submenu_style").last().addClass("current");
});