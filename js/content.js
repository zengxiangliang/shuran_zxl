$(function(){
	/**
	 * 小图hover问题，hover时改变背景图片的background-position。
	 */
	$(".sidebar_list").hover(
		function(){
			$(this).find(".top_icon").css("background-position","-5px -35px");
		},
		function(){
			$(this).find(".top_icon").css("background-position","-5px -65px");
		}
	);
	$("li.sidebar_list").click(function(){
		$(this).css("background-color","#f6f6f6").find("a").addClass("hover");
	});
});